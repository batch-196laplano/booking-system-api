const mongoose = require("mongoose");
//mongoose schema
//schema constructor=====================================
const courseSchema = new mongoose.Schema({
	name:{
		type: String,
		required:[true, "Name is required"]
	},
	description:{
		type: String,
		required:[true, "Description is required"]
	},
	price: {
		type: Number,
		required:[true, "Price is required"]
	},
	isActive:{
		type: Boolean,
		default: true
	},
	createdOn:{
		type: Date,
		default: new Date()
	},
	enrollees:[
		{
			userId:{
				type:String,
				required:[true, "User Id is required"]
			},
			dateEnrolled: {
				type:String,
				default: new Date()
			},
			status:{
				type:String,
				default:"Enrolled"	
			}
		}
	]
})
//========================================================
//module.exports 
module.exports = mongoose.model("Course", courseSchema);