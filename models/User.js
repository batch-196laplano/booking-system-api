const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
	firstname:{
		type: String,
		required:[true, "Firstname is required"]
	},
	lastname:{
		type: String,
		required:[true, "Lastname is required"]
	},
	email:{
		type: String,
		required:[true, "Email is required"]
	},
	password:{
		type: String,
		required:[true, "Password is required"]
	},
	isAdmin:{
		type: Boolean,
		default: false
	},
	mobileNo:{
		type: String,
		required:[true, "Mobie Number is required"]
	},
	enrollments:[
		{
			courseId:{
				type:String,
				required:[true, "Course Id is required"]
			},
			dateEnrolled: {
				type:String,
				default: new Date()
			},
			status:{
				type:String,
				default:"Enrolled"	
			}
		}
	]
})
module.exports = mongoose.model("User", userSchema);