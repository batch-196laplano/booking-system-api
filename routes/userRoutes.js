const express = require("express");
const router = express.Router();

const userControllers = require ("../controllers/userControllers");
// console.log(userControllers);

const auth = require("../auth");
//destructure
const {verify} = auth;

//register
router.post("/",userControllers.registerUser);

router.get("/details",verify,userControllers.getUserDetails);
// 
router.post("/login",userControllers.loginUser);
//activity=========================================
router.post("/checkEmail",userControllers.getEmail);

//user enrollment
//courseId from req.body
//userId from req.user
router.post('/enroll',verify,userControllers.enroll);



module.exports = router;







