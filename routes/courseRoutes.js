//import express	
// Router()method

const express = require("express");
const router = express.Router();
//import the course model
const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;

	router.get('/',courseControllers.getAllCourses);

	router.post('/',verify,verifyAdmin,courseControllers.addCourse);

	router.get('/activeCourses',courseControllers.getActiveCourses);
	//activity========================================================
	// router.post('/getSingleCourse',courseControllers.getSingleCourse);
	//(req.params)>>:id
	router.get('/getSingleCourse/:courseId',courseControllers.getSingleCourse);

	//s38
	router.put("/updateCourse/:courseId",verify,verifyAdmin,courseControllers.updateCourse);

	router.delete("/archiveCourse/:courseId",verify,verifyAdmin,courseControllers.archiveCourse);

	

module.exports = router;