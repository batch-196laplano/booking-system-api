const express = require ("express");
const mongoose = require ("mongoose");
const app = express();
const port = 4000;


mongoose.connect("mongodb+srv://admin:admin123@cluster0.qosmb.mongodb.net/bookingAPI?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true

});

let db = mongoose.connection;
db.on('error',console.error.bind(console, "MongoDB Connection Error."))
db.once('open',()=>console.log("Connected to MongoDB"));

app.use(express.json());

const courseRoutes = require('./routes/courseRoutes');
// console.log(courseRoutes);
app.use('/courses',courseRoutes);

const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);

//used listen()of method to assign a port
app.listen(port,()=> console.log(`Express API running at port 4000`));


