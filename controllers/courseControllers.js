const Course = require("../models/Course");

module.exports.getAllCourses = (req,res)=>{
		// res.send("This route will get all course documents");
		Course.find({})
		.then(result => res.send(result))
		.catch(error => res.send(error))
	}

module.exports.addCourse = (req,res)=>{
		// console.log(req.body);
		//constructor
		let newCourse = new Course ({
			name : req.body.name,
			description: req.body.description,
			price: req.body.price
		})
		// console.log(newCourse);
		newCourse.save()
		.then(result => res.send(result))
		.catch(error => res.send(error))
	}

module.exports.getActiveCourses=(req,res)=>{
	Course.find({isActive:true})
	.then(result =>res.send(result))
	.catch(error => res.send(error))
}

//activity=============================
module.exports.getSingleCourse = (req,res)=>{

	console.log(req.params.courseId);

	// console.log(req.body.id)
		//.find({})//all
		//.findOne({_id:req.body.id})
		Course.findById(req.params.courseId)
		// Course.findOne({_id:req.body.id})
		// console.log(req.body.id);
		.then(result => res.send(result))
		.catch(error => res.send(error))

}
module.exports.updateCourse = (req,res)=>{
	console.log(req.params.courseId);

	console.log(req.body);

	//findByIdAndUpdate(<id>,<{update}>,{new:true})
	let update = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.courseId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}


module.exports.archiveCourse = (req,res)=>{
	let update = {
		isActive: false
	}
	// console.log(req.params.courseId);
	// console.log(req.body);
	// update to inactive for soft delete
	Course.findByIdAndUpdate(req.params.courseId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}








