const User = require("../models/User");
const Course = require("../models/Course")
const bcrypt = require("bcrypt");

const auth = require("../auth");


module.exports.registerUser = (req,res)=>{

	const hashedPw = bcrypt.hashSync(req.body.password,10);
	// console.log(hashedPw);

	let newUser = new User({

		firstname: req.body.firstname,
		lastname: req.body.lastname,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo
	})
		newUser.save()
		.then(result => res.send(result))
		.catch(error => res.send(error))
}

module.exports.getUserDetails = (req,res)=>{
	console.log(req.user)
		//.find({_id:req.body.id})
		//.findOne({_id:req.body.id})
		User.findById(req.user.id)
		// console.log(req.body.id);
		.then(result => res.send(result))
		.catch(error => res.send(error))
}

module.exports.loginUser = (req,res) => {
	console.log(req.body);

//mongdb: db.users.findOne({email:req.body.email})
	User.findOne({email:req.body.email})
	.then(foundUser =>{
		if(foundUser===null){
			return res.send({message: "No User Found."})

		}else{
			// console.log(foundUser)
			//bcrypt.compareSync(input,hashedString)
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);
			// console.log(isPasswordCorrect);
			// if password is correct
			if(isPasswordCorrect){

				//auth.js
				//JSON.webtoken or JWT

			// console.log("We will create a token for the user if the password is correct")
				return res.send({accessToken: auth.createAccessToken(foundUser)})
			}else{
				return res.send({message: "Incorrect Password"});
			}
		}

	})
}

//activity=============================
module.exports.getEmail = (req,res)=>{
	console.log(req.body.email)
		//.find({_id:req.body.id})
		//.findOne({_id:req.body.id})
		User.findOne({email:req.body.email})
		// console.log(req.body.id);
		.then(result => {
			//check if email already exists
			if(result ===null) {
				return res.send(false)
				
			} else {
				return res.send(true)

			}
		})

		.catch(error => res.send(error))
}
//Enrollment============================================================
module.exports.enroll = async (req,res) => {
	// console.log(req.user.id);

	// console.log(req.body.courseId)
	
	if(req.user.isAdmin){
		return res.send({message: "Action Forbidden"});
	}

		// 2 steps in enrollment
		
		//async() and await() to able JS to wait	
		let isUserUpdated = await User.findById(req.user.id).then(user=> {
			// console.log(user)
			let newEnrollment = {
				courseId: req.body.courseId
			}
			//access the enrollments array and push the new enrollment
			user.enrollments.push(newEnrollment)

			//save the user document and return the value
			//catch return true if we push successfully 
			//catch error if otherwise
			return user.save().then(user=>true).catch(err=>err.message)
		})

	// console.log(isUserUpdated);
	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}

	//find the course where we will add the user  as an enrollee
		let isCourseUpdated = await Course.findById(req.body.courseId).then(course=> {
			console.log(course); //contains the found course
			let enrollee = {
				userId: req.user.id
			}

			course.enrollees.push(enrollee)

			return course.save().then(course=>true).catch(err=>err.message)
			// console.log(course); //contains the found course
		})
		// console.log(isCourseUpdated);
		if(isCourseUpdated !== true){
			return res.send({message: isCourseUpdated})
		}
		if(isCourseUpdated&&isUserUpdated){
			return res.send({message: "Thank you for enrolling!"})
		}


	// console.log(isUserUpdated)
}










